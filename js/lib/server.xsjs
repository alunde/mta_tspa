// Note: This xsjs file can be invoked by the following url:
// http://ragnar.lcfx.net:8000/tspa/json/server.xsjs
// POST with request header Content-Type: application/json

// Write a message to the server side log found in the Administration Perspective on the Default Admin Window Diagnosis Files sub-tab.
// Look for a file of the form xsengine_alert_lt5013.trc with recent modification date.
// Run the script with trace statements in it and immediately refresh the log window.
// You'll have to search for your entires because it's logging for the whole server.

 // import math lib
 //$.import("sap.myapp.lib","math");
 
 // use math lib
 //var max_res = $.sap.myapp.lib.math.max(3, 7);

/* Sample POST request (use in Firefox RESTClient)

{"cmds":[
{"cmd":"HRDATA","data":
{
"mfg_did":"78E4-1160D8627D5A",
"hr_data":
[
{"chr":67,"ts":"2013-01-22 01:02:03.456"},
{"chr":89,"ts":"2013-01-22 01:02:03.456"},
{"chr":83,"ts":"2013-01-22 01:02:03.456"},
{"chr":84,"ts":"2013-01-22 01:02:03.456"}
]
}
},
{"cmd":"BGDATA","data":
{
"mfg_did":"BGM007",
"bg_data":
[
{"bg":99,"bam":1,"ts":"2013-01-22 01:02:03.456"},
{"bg":102,"bam":1,"ts":"2013-01-22 01:02:03.456"},
{"bg":131,"bam":2,"ts":"2013-01-22 01:02:03.456"}
]
}
},
{"cmd":"NOTEDATA","data":
{
"mfg_did":"BGM007",
"note_data":
[
{"ts":"2013-01-22 01:02:03.456","note":"O'Reilly's a good man - in a tight spot(semi-colon);"},
{"ts":"2013-01-22 01:02:03.456","note":"Seperate folders with \\ and not / !"},
{"ts":"2013-01-22 01:02:03.456","note":"What's the \"story\" morning glory?"}
]
}
}

],
"ukey":"i830671",
"xmit_ts":"2013-01-22 01:02:03.456"
}

// Example for Benjamin

{"cmds":[
{"cmd":"TEMPDATA","data":
{
"mfg_did":"YourDeviceID",
"unit":"F-or-C",
"temp_data":
[
{"value":99.99,"ts":"2013-05-10 01:02:03.456"},
{"value":100.01,"ts":"2013-05-10 01:02:03.789"},
{"value":101.02,"ts":"2013-05-10 01:02:04.103"}
]
}
}
],
"ukey":"UserID",
"xmit_ts":"2013-05-10 01:02:04.103"
}


 */


//$.trace.debug("request path: " + $.request.getPath());
$.trace.info(":IOT Server START:");

/*
var sql = "";

output += "Here are the 10 most recent items from the HR table. <br> <br>\n";

sql = 'SELECT * FROM "IMPLICARE"."HR" ORDER BY HRID DESC LIMIT 10';

var gconn = $.db.getgconnection();
var pstmt = gconn.prepareStatement(sql);
var rs = pstmt.executeQuery();

var ts = "";

*/
/*
if (!rs.next()) {
	output += "Failed to retrieve data.";
	$.response.status = $.net.http.INTERNAL_SERVER_ERROR;
} else {
	output += "This is the response from my SQL: <br> " + rs.getString(1) + "<br>\n";
}
*/

/*
while (rs.next()) {
	//Doc http://help.sap.com/hana/jsapi/classxsruntime_1_1db_1_1ResultSet.html#a188cf118065a59af62716ddb5d65447e
	ts =  rs.getTimestamp(4).toString('yyyy-MM-dd HH:mm:ss');
	output += "HRID: " + rs.getInteger(1).toString() + " DID: " + rs.getInteger(2).toString() + " CHR: " + rs.getInteger(3).toString() + " TS: " + ts + " <br>\n";
}
 
rs.close();
pstmt.close();
gconn.close();

*/

//Create STATUS codes as necessary
var STATUS = { 
		"OK":0, 
		"UNHANDLED":1, 
		"NOT_OBJECT":100, 
		"NOT_ARRAY":101, 
		"HR_ERROR":201,
		"CO_ERROR":202,
		"LAST_STATUS":999
		};

var output = "";
var debugging = true;  // Turn on debugging to get session messages.

//Check if an object is empty
function is_empty(obj) {

 // Assume if it has a length property with a non-zero value
 // that that property is correct.
 if (obj.length && (obj.length > 0)) {
     return(false);
 }

 if (obj.length && (obj.length === 0)) {
     return(true);
 }

 var key;
 for (key in obj) {
     if (obj.hasOwnProperty(key)) {
        return(false);
     }
 }

 return(true);
}

var gsid = 0;	//Global Session ID
var gconn = {}; //Global DB gconnection
var guid = 0;	//Global User ID
var gdid = 0;	//Global Device ID
var g_xmit_ts = "";	//Global Xmit TS

function getSID() {
	
	var sid = 0;
	var sql = "select \"mta_tspa.db::sId\".NEXTVAL from \"mta_tspa.db::Dummy\"";

	var pstmt = gconn.prepareStatement(sql);
	var rs = pstmt.executeQuery();
	
	if (!rs.next()) {
		sid = 0;
	} else {
		sid = rs.getInteger(1);
	}

	rs.close();
	pstmt.close();

	return(sid);
}

function getUID(ukey) {
	
	var uid = 0;
	var sql = "select \"uId\" from \"mta_tspa.db::data.User\" where \"ukey\" = '" + ukey + "'";

	var pstmt = gconn.prepareStatement(sql);
	var rs = pstmt.executeQuery();
	
	if (!rs.next()) {
		uid = 0;
	} else {
		uid = rs.getInteger(1);
	}

	rs.close();
	pstmt.close();

	return(uid);
}

function getDID(mfg_did) {
	
	var did = 0;
	var sql = "select \"dId\" from \"mta_tspa.db::data.Device\" where \"mfgId\" = '" + mfg_did + "'";

	var pstmt = gconn.prepareStatement(sql);
	var rs = pstmt.executeQuery();
	
	if (!rs.next()) {
		did = 0;
	} else {
		did = rs.getInteger(1);
	}

	rs.close();
	pstmt.close();

	return(did);
}

function logXMIT(did,ts) {
	var sql = "";

	// CTS = '2013-01-28 01:01:01.123'
	sql = "insert into \"mta_tspa.db::data.Transmit\" values(\"mta_tspa.db::xId\".NEXTVAL," + did + ",'" + ts + "',CURRENT_UTCTIMESTAMP)";

	var pstmt = gconn.prepareStatement(sql);
	var result = pstmt.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt.close();
	
}	

function insertMSG(sid,uid,did,msg) {
	var sql = "";

	var escapedmsg = String(msg).replace(/'/g,"\\'");	// Weird, had to explicitly cast msg to String for .replace to work
	
	sql = "insert into \"mta_tspa.db::data.Messages\" values(\"mta_tspa.db::msgId\".NEXTVAL," + sid + "," + uid + "," + did + ",'" + escapedmsg + "',CURRENT_UTCTIMESTAMP)";

	var pstmt = gconn.prepareStatement(sql);
	var result = pstmt.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt.close();
	
}

function logMSG(msg) {
    if (debugging) {
        insertMSG(gsid,guid,gdid,msg);
    }
}


function insertTEMP(did,tempval,utid,ts) {
	var sql = "";

	sql = "insert into \"mta_tspa.db::data.Temp\" values(\"mta_tspa.db::tempId\".NEXTVAL," + did + "," + tempval + ",'" + utid + "','" + ts + "',CURRENT_UTCTIMESTAMP)";

	var pstmt = gconn.prepareStatement(sql);
	var result = pstmt.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt.close();
	
}


//Other functions for handling other types of data
function processTEMPDATA(data) {

/*

{"cmds":[
{"cmd":"TEMPDATA","data":
{
"mfg_did":"YourDeviceID",
"unit":"F-or-C",
"temp_data":
[
{"value":99.99,"ts":"2013-05-10 01:02:03.456"},
{"value":100.01,"ts":"2013-05-10 01:02:03.789"},
{"value":101.02,"ts":"2013-05-10 01:02:04.103"}
]
}
}
],
"ukey":"UserID",
"xmit_ts":"2013-05-10 01:02:04.103"
}

*/
	
	logMSG("Processing TEMP Data");

	var result = {};
	var msg = "";
	
	if (data instanceof Object) {
		var mfg_did = data.mfg_did;
		var unit = data.unit;
		var temp_data = data.temp_data;
		
		var did = getDID(mfg_did);
		
		gdid = did;
		
		var unitid = 0;
		
		if (unit === "C") {	// Celcius
			unitid = 1;
		}
		else {
			if (unit === "F") {	// Farenheit
				unitid = 2;
			}
			else {
				if (unit === "K") {	// Kelvin
					unitid = 3;
				}
				else {
					unitid = 0;
				}
			}
		}
		
		if (temp_data instanceof Array) {
			// logMSG("bg_data is an Array as expected.");
			var i;
			for (i=0; i<temp_data.length; i++) {
				var value = temp_data[i].value;
				var ts = temp_data[i].ts;
				msg += "  temp_data[" + i + "] =  VALUE: " + value + " TS: " + ts + "\n";
				
				insertTEMP(did,value,unitid,ts);
			}
			
			result.status = STATUS.OK;
			result.msg = "OK" + " " + msg;
			logMSG("temp_data processed OK.");
			gdid = 0;
			
		}
		else if (temp_data instanceof Object) {
			logMSG("temp_data is an Object");
			result.status = STATUS.HR_ERROR;
			result.msg = "Error:" + " Expected an array not an object.";
		}
		else {
			logMSG("procTEMP: Not a handled data object.");
			result.status = STATUS.UNHANDLED;
			result.msg = "Not a handled data object";
		}
	}
	else {
		logMSG("procTEMP: expecting object at top level.");
	}
	return(result);
}

function insertDIST(did,distval,utid,ts) {
	var sql = "";

	sql = "insert into \"mta_tspa.db::data.Distance\" values(\"mta_tspa.db::distId\".NEXTVAL," + did + "," + distval + ",'" + utid + "','" + ts + "',CURRENT_UTCTIMESTAMP)";

	var pstmt = gconn.prepareStatement(sql);
	var result = pstmt.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt.close();
	
}

//var gDISTpstmt;

function initDISTS(num_inserts) {
	var sql = "";
	
	var lDISTpstmt;

	sql = "insert into \"mta_tspa.db::data.Distance\" values(\"mta_tspa.db::distId\".NEXTVAL,?,?,?,?,CURRENT_UTCTIMESTAMP)";

	lDISTpstmt = gconn.prepareStatement(sql);
    
    lDISTpstmt.setBatchSize(num_inserts); 
    
    return (lDISTpstmt);
}

function addDIST(pstmt,did,distval,utid,ts) {

    pstmt.setInteger(1,did);     // dId = Integer
    pstmt.setDecimal(2,distval); // temp = Decimal(5,2)
    pstmt.setInteger(3,utid);    // utId = Integer
    pstmt.setTimestamp(4,ts);      // ts = TimeStamp
    
    pstmt.addBatch();  
}

function batchInsertDISTS(pstmt) {
    
    pstmt.executeBatch();  
    
	gconn.commit();	//Don't forget to commit!

	pstmt.close();
}

function initDDATA(num_inserts) {
	var sql = "";
	
	var lDISTpstmt;

	sql = "insert into \"mta_tspa.db::PAL_TRIPLESMOOTH_DATA_TBL\" values(?,?)";

	lDISTpstmt = gconn.prepareStatement(sql);
    
    lDISTpstmt.setBatchSize(num_inserts); 
    
    return (lDISTpstmt);
}

function addDDATA(pstmt,did,distval) {

    pstmt.setInteger(1,did);     // dId = Integer
    pstmt.setFloat(2,distval); // temp = Decimal(5,2)

    pstmt.addBatch();  
}

function batchInsertDDATA(pstmt) {
    
    pstmt.executeBatch();  
    
	gconn.commit();	//Don't forget to commit!

	pstmt.close();
}



//Other functions for handling other types of data
function processDISTDATA(data) {

/*

{"cmds":[
{"cmd":"DISTDATA","data":
{
"mfg_did":"YourDeviceID",
"unit":"E",
"dist_data":
[
{"value":99.99,"ts":"2013-05-10 01:02:03.456"},
{"value":100.01,"ts":"2013-05-10 01:02:03.789"},
{"value":101.02,"ts":"2013-05-10 01:02:04.103"}
]
}
}
],
"ukey":"UserID",
"xmit_ts":"2013-05-10 01:02:04.103"
}

*/
	
	logMSG("Processing DIST Data");

	var result = {};
	var msg = "";
	
	if (data instanceof Object) {
		var mfg_did = data.mfg_did;
		var unit = data.unit;
		var dist_data = data.dist_data;
		
		var did = getDID(mfg_did);
		
		gdid = did;
		
		var unitid = 0;
		
		if (unit === "E") {	// cEntimeters
			unitid = 6;
		}
		else {
			if (unit === "M") {	// Meters
				unitid = 5;
			}
			else {
				unitid = 0;
			}
		}
		
		if (dist_data instanceof Array) {
			// logMSG("bg_data is an Array as expected.");
			var i;
			var psmtDIST;

			if (dist_data.length > 1) {
                psmtDIST = initDISTS(dist_data.length);
			}
			
			msg += "VALUES: " + dist_data.length + " = ";
			
			for (i=0; i<dist_data.length; i++) {
				var value = dist_data[i].value;
				var ts = dist_data[i].ts;
				//msg += "  dist_data[" + i + "] =  VALUE: " + value + " TS: " + ts + "\n";
				msg += value + " : " + "\n";
				
				if (dist_data.length > 1) {
                    addDIST(psmtDIST,did,value,unitid,ts);
                }
                else {
                    insertDIST(did,value,unitid,ts);
                }
			}
			
			if (dist_data.length > 1) {
                batchInsertDISTS(psmtDIST);
			}
			
			logMSG(msg);

			result.status = STATUS.OK;
			result.msg = "OK" + " " + msg;
			logMSG("dist_data processed OK.");
			gdid = 0;
			
		}
		else if (dist_data instanceof Object) {
			logMSG("dist_data is an Object");
			result.status = STATUS.HR_ERROR;
			result.msg = "Error:" + " Expected an array not an object.";
		}
		else {
			logMSG("procDIST: Not a handled data object.");
			result.status = STATUS.UNHANDLED;
			result.msg = "Not a handled data object";
		}
	}
	else {
		logMSG("procDIST: expecting object at top level.");
	}
	return(result);
}


function insertNOTE(uid,ts,msg) {
	var sql = "";

	var escapedmsg = String(msg).replace(/'/g,"''");	// Weird, had to explicitly cast msg to String for .replace to work
	
	sql = "insert into \"mta_tspa.db::data.Note\" values(\"mta_tspa.db::noteId\".NEXTVAL," + uid + ",'" + escapedmsg + "','" + ts + "',CURRENT_UTCTIMESTAMP)";
	
	//logMSG("insertNOTE SQL: " + String(sql).replace(/'/g,"\\'"));
	//logMSG("insertNOTE SQL: " + sql);
	$.trace.info(":IOT Server insertNOTE SQL:" + sql + ":");

	var pstmt = gconn.prepareStatement(sql);
	var result = pstmt.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt.close();
	
}


//Define needed functions to process various types of data
function processNOTEDATA(data) {
	
	// Expecting data to be of the following form:
/*	
		{
		"ukey":"ABCDEF",
		"cmds":[
		{"cmd":"NOTES","note_data":{"notes":[{"note":"This is a note.","ts":"2013-01-02 01:02:03.456"},{"note":"And so is this.","ts":"2013-01-09 01:02:03.456"}]}}
		],
		"xmit_ts":"2013-01-02 01:02:03.456"
		}
*/

	logMSG("Processing NOTE Data");

	var result = {};
	var msg = "";
	
	if (data instanceof Object) {
		var note_data = data.note_data;
		
		if (note_data instanceof Array) {
			// logMSG("note_data is an Array as expected.");
			var i;
			for (i=0; i<note_data.length; i++) {
				var ts = note_data[i].ts;
				var note = note_data[i].note;
				msg += "  note_data[" + i + "] =  TS: " + ts + " NOTE: " + note + "\n";
				
				insertNOTE(guid,ts,note);
			}
			
			result.status = STATUS.OK;
			result.msg = "OK" + " " + msg;
			logMSG("note_data processed OK.");
			gdid = 0;
			
		}
		else if (note_data instanceof Object) {
			logMSG("note_data is an Object");
			result.status = STATUS.HR_ERROR;
			result.msg = "Error:" + " Expected an array not an object.";
		}
		else {
			logMSG("procNOTE: Not a handled data object.");
			result.status = STATUS.UNHANDLED;
			result.msg = "Not a handled data object";
		}
	}
	else {
		logMSG("procNOTE: expecting object at top level.");
	}
	return(result);
}

//NULL functions for handling Monitoring functions
function processNULLDATA(data) {
	// Expecting data to be of the following form:
	/*	
	 * http://michanadev.phl.sap.corp:8000/sap/mic/idc/server.xsjs?json={%22ukey%22:%22NULL%22,%22cmds%22:[{%22cmd%22:%22NULL%22,%22data%22:{}}],%22xmit_ts%22:%222013-01-02%2001:02:03.456%22}
		{"ukey":"NULL","cmds":[{"cmd":"NULL","data":{}}],"xmit_ts":"2013-01-02 01:02:03.456"}
	*/


	var result = {};
	var msg = "";
	
	msg = "processNULLDATA.";

	logMSG(msg);
		
	result.status = STATUS.OK;
	result.msg = "OK" + " " + msg;
	return(result);
}

//Other functions for handling other types of data
function processOtherDATA(data) {
	var result = {};
	var msg = "";
	
	msg = "processOtherDATA is not yet implemented.";

	logMSG(msg);
		
	result.status = STATUS.OK;
	result.msg = "OK" + " " + msg;
	return(result);
}

// ==== Run the Predictive Analytics 
 
// PAL_FCTRIPLESMOOTH_CONTROL_TBL      -> PAL_TRIPLESMOOTH_CONTROL_TBL
// PAL_FCTRIPLESMOOTH_DATA_TBL         -> PAL_TRIPLESMOOTH_DATA_TBL
// PAL_FCTRIPLESMOOTH_OUTPARAMETER_TBL -> PAL_TRIPLESMOOTH_STATISTICS_TBL (PAL_TRIPLESMOOTH_STATISTICS_T)
// PAL_FCTRIPLESMOOTH_PDATA_TBL        -> PAL_TRIPLESMOOTH_PDATA_TBL
// PAL_FCTRIPLESMOOTH_RESULT_TBL       -> PAL_TRIPLESMOOTH_RESULT_TBL

function predictDISTDATA() {
    var result = {};
    var sql = "";
    var pstmt;
    var pcall;
    var rs;
    var sql_result;
    
    var avg_ms_between_vals;
    //var look_at = 24;
    //var look_at = 40;
    //var look_at = 80;
    //var look_at = 30;
    var look_at = 20;

	var msg = "";
	
	var ts = 0;
	var distval = 0.0;
	
	var distidx = 0;
	
	var var_variable = "";
	var var_table = "";

	var var_name;
	var var_value;
	
	var pred_time;
	var pred_value;

	// Find out the difference in time between samples so that we know if there are time gaps in the data
	
	msg = "predictDISTDATA....BEGIN."; logMSG(msg);

	msg = "getting avg_ms_between."; logMSG(msg);

    sql = "SELECT TOP 1 ((NANO100_BETWEEN(MIN(\"ts\"),MAX(\"ts\")) / COUNT(\"ts\")) / 10000) AS AVG_MS_BETWEEN_VALUES FROM \"mta_tspa.db::data.Distance\" WHERE \"distId\" > ((SELECT MAX(\"distId\") AS DID FROM \"mta_tspa.db::data.Distance\") - " + look_at + ")";

	pstmt = gconn.prepareStatement(sql);
	rs = pstmt.executeQuery();
	
	if (!rs.next()) {
		avg_ms_between_vals = 0.0;
	} else {
		avg_ms_between_vals = rs.getFloat(1);
	}

	rs.close();
	pstmt.close();

	msg = "clearing pred data table."; logMSG(msg);
	
    sql = "DELETE FROM \"mta_tspa.db::PAL_TRIPLESMOOTH_DATA_TBL\"";
    
	pstmt = gconn.prepareStatement(sql);
	sql_result = pstmt.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt.close();


	msg = "copying last " + look_at + " values of dist data to DATA_TBL."; logMSG(msg);

    sql  = "SELECT \"mta_tspa.db::pred\".NEXTVAL AS TIMESTAMP,	\"distval\" AS VALUE ";
    sql += "FROM \"mta_tspa.db::data.Distance\" ";
    sql += "WHERE \"distId\" > ((SELECT MAX(\"distId\") AS DID FROM \"mta_tspa.db::data.Distance\") - " + look_at + ")";

	pstmt = gconn.prepareStatement(sql);
	rs = pstmt.executeQuery();
	
	result.distvals = [];
	distidx = 0;
	
	var psmtDDATA = initDDATA(look_at);

	while (rs.next()) {
		ts = rs.getInteger(1);
		distval = rs.getFloat(2);
		result.distvals.push({TIME: distidx, VALUE: distval});
		addDDATA(psmtDDATA, distidx, distval);
		distidx++;
	}

    batchInsertDDATA(psmtDDATA);

	rs.close();
	pstmt.close();

// Example from the docs where the only change is the SCHEMA        
//-- 
//-- Online Docs for Triple Exponential Smoothing
//-- http://help.sap.com/saphelp_hanaplatform/helpdata/en/a2/5b1dee883a4bc4a984bf9496c7a954/content.htm?frameset=/en/9a/04141bd70b44e3bc3aab99abd6c7de/frameset.htm&current_toc=/en/32/731a7719f14e488b1f4ab0afae995b/plain.htm&node_id=63
//-- NOTE: This SQL MUST be run by the TSPA_USER in an SQL console at least once in order to set up the tables in the DB.
//
//SET SCHEMA TSPA;
//
//DROP TYPE PAL_TRIPLESMOOTH_DATA_T;
//CREATE TYPE PAL_TRIPLESMOOTH_DATA_T AS TABLE("ID" INT, "RAWDATA" DOUBLE);
//
//DROP TYPE PAL_CONTROL_T;
//CREATE TYPE PAL_CONTROL_T AS TABLE ("NAME" VARCHAR (100), "INTARGS" INT, "DOUBLEARGS" DOUBLE, "STRINGARGS" VARCHAR (100));
//
//DROP TYPE PAL_TRIPLESMOOTH_RESULT_T;
//CREATE TYPE PAL_TRIPLESMOOTH_RESULT_T AS TABLE ("TIME" INT, "OUTPUT" DOUBLE);
//
//DROP TYPE PAL_TRIPLESMOOTH_STATISTICS_T;
//CREATE TYPE PAL_TRIPLESMOOTH_STATISTICS_T AS TABLE ("NAME" VARCHAR (50), "VALUE" DOUBLE);
//
//DROP TABLE PAL_TRIPLESMOOTH_PDATA_TBL;
//CREATE COLUMN TABLE PAL_TRIPLESMOOTH_PDATA_TBL("POSITION" INT, "SCHEMA_NAME" NVARCHAR(256), "TYPE_NAME" NVARCHAR(256), "PARAMETER_TYPE" VARCHAR(7));
//INSERT INTO PAL_TRIPLESMOOTH_PDATA_TBL VALUES (1,'TSPA', 'PAL_TRIPLESMOOTH_DATA_T','IN');
//INSERT INTO PAL_TRIPLESMOOTH_PDATA_TBL VALUES(2,'TSPA', 'PAL_CONTROL_T','IN'); 
//INSERT INTO PAL_TRIPLESMOOTH_PDATA_TBL VALUES(3,'TSPA', 'PAL_TRIPLESMOOTH_RESULT_T','OUT');
//INSERT INTO PAL_TRIPLESMOOTH_PDATA_TBL VALUES(4,'TSPA', 'PAL_TRIPLESMOOTH_STATISTICS_T','OUT');
//
//CALL SYS.AFLLANG_WRAPPER_PROCEDURE_DROP('TSPA', 'TRIPLESMOOTH_TEST_PROC'); 
//CALL SYS.AFLLANG_WRAPPER_PROCEDURE_CREATE('AFLPAL', 'TRIPLESMOOTH', 'TSPA', 'TRIPLESMOOTH_TEST_PROC',PAL_TRIPLESMOOTH_PDATA_TBL); 
//
//-- This DROP will fail the first time in a session
//-- DROP TABLE #PAL_CONTROL_TBL;
//-- Note: In the example this is a TEMP TABLE called #PAL_CONTROL_TBL but we will use a normal one called PAL_TRIPLESMOOTH_CONTROL_TBL to preserve the settings.
//DROP TABLE PAL_TRIPLESMOOTH_CONTROL_TBL;
//-- CREATE LOCAL TEMPORARY COLUMN TABLE #PAL_CONTROL_TBL ("NAME" VARCHAR(100), "INTARGS" INT, "DOUBLEARGS" DOUBLE, "STRINGARGS" VARCHAR(100));
//
//-- Note: There is no functionality in this code to adjust the CONTROL_TBL values.  UPDATE the PAL_TRIPLESMOOTH_CONTROL_TBL in the SQL console.
//
//CREATE COLUMN TABLE PAL_TRIPLESMOOTH_CONTROL_TBL ("NAME" VARCHAR(100), "INTARGS" INT, "DOUBLEARGS" DOUBLE, "STRINGARGS" VARCHAR(100));
//INSERT INTO PAL_TRIPLESMOOTH_CONTROL_TBL VALUES ('ALPHA', NULL, 0.822, NULL);
//INSERT INTO PAL_TRIPLESMOOTH_CONTROL_TBL VALUES ('BETA', NULL, 0.055, NULL);
//INSERT INTO PAL_TRIPLESMOOTH_CONTROL_TBL VALUES ('GAMMA', NULL, 0.055, NULL);
//INSERT INTO PAL_TRIPLESMOOTH_CONTROL_TBL VALUES ('CYCLE', 4, NULL, NULL);
//INSERT INTO PAL_TRIPLESMOOTH_CONTROL_TBL VALUES ('FORECAST_NUM', 6, NULL, NULL);
//INSERT INTO PAL_TRIPLESMOOTH_CONTROL_TBL VALUES ('SEASONAL', 0, NULL, NULL);
//INSERT INTO PAL_TRIPLESMOOTH_CONTROL_TBL VALUES ('INITIAL_METHOD', 0, NULL, NULL);
//INSERT INTO PAL_TRIPLESMOOTH_CONTROL_TBL VALUES ('MEASURE_NAME', NULL, NULL, 'MSE');
//INSERT INTO PAL_TRIPLESMOOTH_CONTROL_TBL VALUES ('EXPOST_FLAG', 1, NULL, NULL);
//
//DROP TABLE PAL_TRIPLESMOOTH_DATA_TBL;
//CREATE COLUMN TABLE PAL_TRIPLESMOOTH_DATA_TBL LIKE PAL_TRIPLESMOOTH_DATA_T ;
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (1,362.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (2,385.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (3,432.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (4,341.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (5,382.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (6,409.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (7,498.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (8,387.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (9,473.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (10,513.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (11,582.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (12,474.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (13,544.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (14,582.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (15,681.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (16,557.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (17,628.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (18,707.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (19,773.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (20,592.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (21,627.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (22,725.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (23,854.0);
//INSERT INTO PAL_TRIPLESMOOTH_DATA_TBL VALUES (24,661.0);
//
//DROP TABLE PAL_TRIPLESMOOTH_RESULT_TBL;
//CREATE COLUMN TABLE PAL_TRIPLESMOOTH_RESULT_TBL LIKE PAL_TRIPLESMOOTH_RESULT_T;
//
//DROP TABLE PAL_TRIPLESMOOTH_STATISTICS_TBL;
//CREATE COLUMN TABLE PAL_TRIPLESMOOTH_STATISTICS_TBL LIKE PAL_TRIPLESMOOTH_STATISTICS_T;
//
//CALL TSPA.TRIPLESMOOTH_TEST_PROC(PAL_TRIPLESMOOTH_DATA_TBL, "#PAL_CONTROL_TBL", PAL_TRIPLESMOOTH_RESULT_TBL, PAL_TRIPLESMOOTH_STATISTICS_TBL) WITH OVERVIEW;
//
//SELECT * FROM PAL_TRIPLESMOOTH_RESULT_TBL;
//SELECT * FROM PAL_TRIPLESMOOTH_STATISTICS_TBL;

    if (true) {
        
        // Not needed for XS-Advancted apps
        // gconn.prepareStatement("SET SCHEMA \"TSPA\"").execute();
        
        msg = "Clear the ouput tables."; logMSG(msg);
        
        if (false) {
            // Don't have to DROP if it's a TEMP TABLE
            // sql = "DROP TABLE #PAL_TRIPLESMOOTH_STATISTICS_TBL";
             
            // pstmt = gconn.prepareStatement(sql);
            // sql_result = pstmt.execute();
        
            // //gconn.commit();	//Don't forget to commit!
        
            // pstmt.close();
        
            //sql = "CREATE LOCAL TEMPORARY COLUMN TABLE #PAL_TRIPLESMOOTH_STATISTICS_TBL (\"NAME\" VARCHAR(50), \"VALUE\" DOUBLE)";
            sql = 'DELETE FROM "mta_tspa.db::PAL_TRIPLESMOOTH_STATISTICS_TBL"';
    
            pstmt = gconn.prepareStatement(sql);
            sql_result = pstmt.execute();
        
            //gconn.commit();	//Don't forget to commit!
        
            pstmt.close();
        
        
            // sql = "DROP TABLE #PAL_TRIPLESMOOTH_RESULT_TBL";
    
            // pstmt = gconn.prepareStatement(sql);
            // sql_result = pstmt.execute();
        
            // //gconn.commit();	//Don't forget to commit!
        
            // pstmt.close();
        
            //sql = "CREATE LOCAL TEMPORARY COLUMN TABLE #PAL_TRIPLESMOOTH_RESULT_TBL (\"TIME\" INT, \"OUTPUT\" DOUBLE)";
        	sql = 'DELETE FROM "mta_tspa.db::PAL_TRIPLESMOOTH_RESULT_TBL"';
    
            pstmt = gconn.prepareStatement(sql);
            sql_result = pstmt.execute();
        
            //gconn.commit();	//Don't forget to commit!
        
            pstmt.close();
        }
    
        // Now call the predictive routine.
        
        if (true) {

            //sql = "CALL _SYS_AFL.TSPA_FCTRIPLESMOOTH_TEST_PROC(TSPA.PAL_TRIPLESMOOTH_DATA_TBL, TSPA.PAL_FCTRIPLESMOOTH_CONTROL_TBL, #PAL_TRIPLESMOOTH_STATISTICS_TBL, #PAL_FCTRIPLESMOOTH_RESULT_TBL) WITH OVERVIEW";
            //     CALL TSPA.TRIPLESMOOTH_TEST_PROC(PAL_TRIPLESMOOTH_DATA_TBL, "#PAL_CONTROL_TBL", PAL_TRIPLESMOOTH_RESULT_TBL, PAL_TRIPLESMOOTH_STATISTICS_TBL) WITH OVERVIEW;
            //sql = 'CALL TRIPLESMOOTH_TEST_PROC(PAL_TRIPLESMOOTH_DATA_TBL, PAL_TRIPLESMOOTH_CONTROL_TBL, "#PAL_TRIPLESMOOTH_RESULT_TBL", "#PAL_TRIPLESMOOTH_STATISTICS_TBL") WITH OVERVIEW';
            sql = 'CALL "mta_tspa.db::PAL_TRIPLESMOOTH"("mta_tspa.db::PAL_TRIPLESMOOTH_DATA_TBL", "mta_tspa.db::PAL_TRIPLESMOOTH_CONTROL_TBL", ?, ?)';

            //pcall = gconn.prepareStatement(sql);
            pcall = gconn.prepareCall(sql);
            sql_result = pcall.execute();
            
            ///msg = "";
            
            if(pcall.execute()) { 
        		//msg += "total_input_rows: " + pcall.getBigInt(4) + "\n"; 
         
	            rs = pcall.getResultSet(); 
	            //msg += "=========\n"; 
	            //msg += "ResultSet has: " + rs.getMetaData().getColumnCount() + " column(s)\n"; 
				result.predvalues = [];
	            while(rs.next()) { 
		    		pred_time = rs.getInteger(1);
		    		pred_value = rs.getFloat(2);
		    		result.predvalues.push({TIME: pred_time, VALUE: pred_value});
	            } 
	            //msg += "=========\n\n"; 
	            pcall.getMoreResults();

	            rs = pcall.getResultSet(); 
	            //msg += "=========\n"; 
	            //msg += "ResultSet has: " + rs.getMetaData().getColumnCount() + " column(s)\n"; 
				result.predparams = [];
	            while(rs.next()) { 
		    		var_name = rs.getString(1);
		    		var_value = rs.getString(2);
		    		result.predparams.push({NAME: var_name, VALUE: var_value});
	            } 
	            //msg += "=========\n\n"; 
	            pcall.getMoreResults();

		    } else { 
		        msg = "Failed to execute procedure"; logMSG(msg);
		    } 
        // INSPECCT sql_result!!!
            //gconn.commit();	//Don't forget to commit!
        
            pcall.close();
        }
    }


    // Read the predicted data

    if (false) {
		result.predparams = [];
        sql = "SELECT * FROM #PAL_TRIPLESMOOTH_STATISTICS_TBL";
    
    	pstmt = gconn.prepareStatement(sql);
    	rs = pstmt.executeQuery();
    	
    	while (rs.next()) {
    		var_name = rs.getString(1);
    		var_value = rs.getString(2);
    		result.predparams.push({NAME: var_name, VALUE: var_value});
    	}
    
    	rs.close();
    	pstmt.close();
    }

    if (false) {
		result.predvalues = [];
        sql = "SELECT * FROM #PAL_TRIPLESMOOTH_RESULT_TBL";
    
    	pstmt = gconn.prepareStatement(sql);
    	rs = pstmt.executeQuery();
    	
    	while (rs.next()) {
    		pred_time = rs.getInteger(1);
    		pred_value = rs.getFloat(2);
    		result.predvalues.push({TIME: pred_time, VALUE: pred_value});
    	}
    
    	rs.close();
    	pstmt.close();
    }


	msg = "predictDISTDATA....END."; logMSG(msg);
    
	result.status = STATUS.OK;
	result.look_at = look_at;
	result.avg_ms_between_vals = avg_ms_between_vals;
	
    return(result);
}

//Doesn't work with JSON.parse (object member names must be double quoted.)
//var strJSON = '{cmd:"HRDATA",data:[{chr:78},{chr:79},{chr:82}]}';

//Works with JSON.parse
var strJSON = '{"cmd":"HRDATA","data":[{"chr":78},{"chr":79},{"chr":82}]}';

// This script is expecting a POST with the following format.
/*

Content-Type: application/json

{
"ukey":"ABCDEF",
"cmds":[
{"cmd":"HRDATA","data":{"mfg_did":"78E4-1160D8627D5A","hr_data":[{"chr":67,"ts":"2013-01-02 01:02:03.067","rr_data":[987,1123]},{"chr":89,"ts":"2013-01-02 01:02:03.089","rr_data":[987,1123,1123]},{"chr":83,"ts":"2013-01-02 01:02:03.083","rr_data":[]},{"chr":84,"ts":"2013-01-02 01:02:03.084","rr_data":[1187]}]}},
{"cmd":"BGDATA","data":{"mfg_did":"BGM007","bg_data":[{"bg":99,"bam":"B","ts":"2013-01-02 01:02:03.456"}]}},
{"cmd":"NOTES","data":{"notes":[{"note":"This is a note.","ts":"2013-01-02 01:02:03.456"},{"note":"And so is this.","ts":"2013-01-09 01:02:03.456"}]}}
],
"xmit_ts":"2013-01-02 01:02:03.456"
}

*/


try {
	// Always assume and exception can occur

	var i=0;
	
	//var request_method = $.request.getMethod();	// Deprecated syntax doesn't work!?
	var request_method = $.request.method;
	var method = "";
	
	method += $.request.toString();

	switch(request_method) {
	case $.net.http.GET:	// GET == 1
		method = "GET";
		//var jsonParam = $.request.getParameter("json"); // Deprecated syntax doesn't work!
		strJSON = "";
		// Parameters from the HTTP GET query string
		for (i=0; i<$.request.parameters.length; ++i) {
		    var name = $.request.parameters[i].name;
		    var value = $.request.parameters[i].value;
		    if (name === "json") {
				strJSON = value;
				// method += "json: " + value;
		    }
		    else {
				method += " only param named json accepted. not " + name;
		    }
		}
		if (strJSON !== "") {
			method += " " + strJSON;
		}
		else {
			method += " empty param named json.";
		}
		break;
	case 2: // Unknown
		method = "Unknown";
		break;
	case $.net.http.POST: // POST = 3
		method = "POST";
		
		// Loop through all the HTTP headers and only process the body if content-type is application/json
		var content_type = "";
		for (i=0; i<$.request.headers.length; ++i) {
		    var name = $.request.headers[i].name;
		    var value = $.request.headers[i].value;

		    //method += " header[" + i + "] = " + name + ":" + value + "\n";

		    if (name === "content-type") {
				content_type = value;
			    method += " header[" + i + "] = " + name + ":" + value + "\n";
		    }
 
		}
		if (content_type.indexOf("application/json") !== -1) {
			// Extract the body of the POST
			strJSON = $.request.body.asString();
			//method += " POSTED BODY: " + strJSON;
		}
		else {
			method += "\n ERROR: expected content-type:applicaton/json.";
		}
		
		break;
	case $.net.http.PUT: // PUT = 4
		method = "PUT";
		break;
	case $.net.http.DEL: // DELETE = 5
		method = "DELETE";
		break;
	case 0: // OPTIONS
		method = "OPTIONS";
		break;
	case $.net.http.gconnECT: // gconnECT = 7
		method = "gconnECT";
		break;
	default:
		method = "Unhandled";
		break;
	}

	output = "IOT JSON Server : " + method + "\n\n";

	gconn = $.db.getConnection();
	
	gsid = getSID();	// Get a unique Session ID for this session.  Do this before calling log()
	
	logMSG("Session ========== " + gsid + " ========== BEGIN.");
	
	//EVAL not allowed.
	//var objJSON = eval("(function(){return " + strJSON + ";})()");

	//Works (explicit declaration of object)
	//var objJSON = {cmd:"HRDATA","data":[{chr:78},{chr:79},{chr:82}]};

	// Parse the JSON string into an object
	var objJSON = JSON.parse(strJSON);

	//alert("JSON: \n\n" + strJSON);

	output += "strJSON = " + strJSON + "\n";

	//output += "objJSON = " + objJSON + "\n";
	
	output += "\n";	

	var propt;
	var objResult = {};

	var predResult = {};

	objResult.status = STATUS.OK;

	if (objJSON instanceof Object) {
		
		guid = getUID(objJSON.ukey);

		var xmit_ts = objJSON.xmit_ts;
		
		g_xmit_ts = xmit_ts;
		
		//var xmit_ts = "2013-01-02 01:02:03.456";
		logXMIT(guid,xmit_ts);
		
		var cmds = objJSON.cmds;
		
		if (cmds instanceof Array) {
			var objCMD = {};
			for (i=0; i<cmds.length; i++) {
				output += "  cmds[" + i + "].cmd = " + cmds[i].cmd + "\n";
				objCMD = cmds[i];

				for(propt in objCMD){
					if(objCMD.hasOwnProperty(propt)) {
						output += "propt: " + propt + " = " + objCMD[propt] + "\n";
						
						if (propt === "cmd") {
    output += "data: " + " = " + objCMD.data + "\n";
							// Figure out which command it is
							if (objCMD.cmd === "NOTEDATA") {
							    // Note Data
								output += "NOTE: " + processNOTEDATA(objCMD.data);
							}
							else if (objCMD.cmd === "TEMPDATA") {
								// Temperature Data
								output += "TEMP: " + processTEMPDATA(objCMD.data);
							}
							else if (objCMD.cmd === "DISTDATA") {
								// Distance Data
								output += "DIST: " + processDISTDATA(objCMD.data);
								predResult = predictDISTDATA();
							}
							else if (objCMD.cmd === "NULL") {
								// Note Data
								output += "NULL: " + processNULLDATA(objCMD.data);
							}
							else {
								// Command not handled
								output += "Other: " + processOtherDATA(objCMD.data);
							}
						}
					}
				}
				
				output += "cmd = " + objCMD.cmd + "\n";
			
			}
		}
		else if (cmds instanceof Object) {
			output += "Error:" + " Expected an array not an object.";
		}
		else {
			output += "Not a handled data object";
		}
		
	}
	else {
		// Expected top level JSON element to be an object(containing an array of cmd objects)
		output += "Error:" + " Expected an object at the top level.";
	}
	

	output += "\nFINISHED... \n";
	
	if ((!(is_empty(predResult))) || (!(is_empty(objResult)))) {
		$.response.contentType = "application/json";
		if (!(is_empty(predResult))) {
            $.response.setBody(JSON.stringify(predResult));
		}
        else {
            objResult.msg = "\n" + output;
            objResult.svrts = "Server TimeStamp";
            objResult.chunk = "Chunk Size Requested";
            objResult.inter = "Interval Requested";
	
            $.response.setBody(JSON.stringify(objResult));
        }
	}
	else {
		$.response.contentType = "text/html";
		$.response.setBody(output);
	}
	
	logMSG("Session ========== " + gsid + " ========== END.");
	
} catch (exception) {
	// Figure out what kind of exception it is and handle ones we care about.
	if (exception instanceof TypeError) {
		// Handle TypeError exception
		output += "TypeError: " + exception.toString() + "\n";
	} else if (exception instanceof ReferenceError) {
		// Handle ReferenceError
		output += "ReferenceError: " + exception.toString() + "\n";
	} else if (exception instanceof SyntaxError) {
		// Handle SyntaxError
		output += "SyntaxError: " + exception.toString() + "\n";
	} else {
		// Handle all other not handled exceptions
		output += "ExceptionError: " + exception.toString() + "\n";
	}
	
	$.response.contentType = "text/html";
	$.response.setBody(output);
	
} finally {
	// This code is always executed even if an exception is 
	if (gconn instanceof Object) {
		gconn.close();
	}
}

$.trace.info(":IOT Server END:");


